# Script de backup de base PostgreSQL

Parâmetros do script
  * 1º usuario `obrigatorio`
  * 2º host `obrigatorio`
  * 3º base `obrigatorio`
  * 4º "path bucket s3" `opcional`

 Exemplo:
```shell
  sh backupBasePostgres.sh postgres 127.0.0.1 base_teste bucket
```
Datelhe no ultimo parâmetro quando informado, aciona a função de envio para a S3 para o bucket especificado no parâmetro.

O path de destino dos backups que vai ser utilizado é em `/mnt/backupsdodia`, se quiser modificar, basta atualizar a variável `$DEST` no script.

Se o seu banco PostgreSQL for uma versão inferior a 9.6, você deve mudar a variável `$PG` de `/usr/pgsql-9.6/bin` para `/usr/bin`.

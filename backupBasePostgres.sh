#!/bin/bash

# Creation date: 05/10/2017
# Author: Álvaro Bacelar
# Email: alvaro@alvarobacelar.com

# Vars script
PGUSER=$1 # user of conect on database
PGHOST=$2 # host of conect on database
BASE=$3 # base for backup
PATHS3=$4 # path remote storage (s3 bucket)
PGDUMPOPTS="-v -F c -b -h" # options dump
PG="/usr/pgsql-9.6/bin"
PGSQL="$PG/psql" # path of exection the psql
PGDUMP="$PG/pg_dump" # path of exection the pg_dump
TAR="$(which tar)"
DEST="/mnt/backupsdodia" # path destination of dumps
NOW="$(date +"%Y%m%d-%H%M")" # date and hour the backup
FILEDUMP="Backup-$BASE-$NOW.backup" # name file dump
FILETAR="Backup-$BASE-$NOW.tar.gz" # name file dump compact

backupBase(){
  # checking for vars dump
  if [ -n $PGUSER -a -n $PGHOST -a -n $BASE ]; then
    echo "$(date) - Iniciando o script de backup do dados $BASE"
    # Create path if not exist
    if [ ! -e "$DEST" ]; then
      echo "$(date) - Criando o diretorio $DEST"
      mkdir -p $DEST
    fi
    # Checks if the path has created
    if [ $? -ne 0 ]; then
      echo "$(date) - Falha ao criar o diretório $DEST"
      exit 1
    fi
    # go to directory
    cd $DEST
    # test connection with the database
    echo "\list" | $PGSQL -U $PGUSER -h $PGHOST $BASE
    # quit with error if there is an error
    if [ $? -ne 0 ]; then
      echo "$(date) - Não foi possivel se conectar com o banco postgres"
      exit 1
    fi
    # start dump the database
    echo "$(date) - Iniciando o dump da base de dados $BASE"
    $PGDUMP -U $PGUSER $PGDUMPOPTS $PGHOST -f "$FILEDUMP" $BASE
    # quit with error if there is an error
    if [ $? -ne 0 ] ; then
      echo "$(date) - Falha ao realizar o dump da base $BASE"
      rm -rf $FILEDUMP
      exit 1
    fi
    # compact file dump
    echo "$(date) - Realizando a compactação da base $BASE"
    $TAR czf $FILETAR $FILEDUMP
    # quit with error if there is an error
    if [ $? -ne 0 ] ; then
      echo "$(date) - Falha ao compactar o arquivo de backup $FILEDUMP"
      rm -rf $FILETAR
      exit 1
    fi
    # remove file dump after of compact
    rm -rf $FILEDUMP
    # checking if the path remote has informed
    if [ -z $PATHS3 ]; then
      # remove the file of backup
      echo "$(date) - Backup da base $BASE finalizado com sucesso!"
    else
      sendFileS3
    fi

  else
    echo "$(date) - Não foi possivel realizar o backup, não foi informado os parametros necessários: usuario host base"
    exit 1
  fi
}

sendFileS3(){
  # set vars destination storage aws s3
  YEAR="$(date +"%Y")"
  MONTH="$(date +"%m")"
  AWS="$(which aws)"
  PATHSRCS3="s3://$PATHS3/$YEAR/$MONTH/"
  # send file for remote storage s3
  echo "$(date) - Enviando o arquivo $FILETAR para o dietorio $PATHSRCS3"
  $AWS s3 cp $FILETAR $PATHSRCS3
  if [ $? -ne 0 ] ; then
    rm -rf $FILEDUMP
    echo "$(date) - Falha ao enviar o arquivo $FILETAR para o diretorio $PATHSRCS3"
    exit 1
  fi
  # remove the file of backup
  rm -rf $FILEDUMP
  echo "$(date) - Backup da base $BASE finalizado com sucesso!"
}

# start script with function backupBase
backupBase
